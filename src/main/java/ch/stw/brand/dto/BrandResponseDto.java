package ch.stw.brand.dto;

import jakarta.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.UUID;

/**
 * DTO for {@link ch.stw.brand.entity.Brand}
 */
public record BrandResponseDto(@NotNull UUID id, @NotNull String name) implements Serializable
{
}
