package ch.stw.brand.dto;

import java.io.Serializable;

/**
 * DTO for {@link ch.stw.brand.entity.Brand}
 */
public record BrandUpdateDto(String name)
    implements Serializable
{
}
