package ch.stw.brand.repository;

import ch.stw.brand.entity.Brand;
import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;
import jakarta.enterprise.context.ApplicationScoped;
import java.util.UUID;

@ApplicationScoped
public class BrandRepository implements PanacheRepositoryBase<Brand, UUID>
{
}
