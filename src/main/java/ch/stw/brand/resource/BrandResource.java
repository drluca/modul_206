package ch.stw.brand.resource;

import ch.stw.brand.dto.BrandCreateDto;
import ch.stw.brand.dto.BrandResponseDto;
import ch.stw.brand.dto.BrandUpdateDto;
import ch.stw.brand.service.BrandService;
import jakarta.annotation.security.RolesAllowed;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.PATCH;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Response;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;

@Path("brands")
@ApplicationScoped
@RequiredArgsConstructor
public class BrandResource
{

    private final BrandService brandService;

    /**
     * Retrieves a list of brands.
     *
     * @return The list of brands as a Response object.
     */
    @GET
    @Produces("application/json")
    @RolesAllowed({"USER", "ADMIN"})
    public Response getBrands()
    {
        List<BrandResponseDto> brands = brandService.getBrands();
        return Response.ok(brands).build();
    }

    /**
     * Retrieves a brand by its ID.
     *
     * @param id The ID of the brand.
     * @return The brand as a Response object.
     */
    @GET
    @Path("{id}")
    @Produces("application/json")
    @RolesAllowed({"USER", "ADMIN"})
    public Response getBrand(@PathParam("id") UUID id)
    {
        BrandResponseDto brand = brandService.getBrand(id);
        return Response.ok(brand).build();
    }

    /**
     * Creates a new brand.
     *
     * @param brandCreateDto The brand creation DTO.
     * @return The response containing the created brand resource.
     */
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @RolesAllowed("ADMIN")
    public Response createBrand(BrandCreateDto brandCreateDto)
    {
        return Response.created(brandService.createBrand(brandCreateDto)).build();
    }

    /**
     * Updates a brand with the specified ID.
     *
     * @param id       The ID of the brand to update.
     * @param brandDto The BrandUpdateDto object containing the updated brand information.
     * @return A Response object with the updated brand as the entity.
     */
    @PATCH
    @Path("{id}")
    @Consumes("application/json")
    @Produces("application/json")
    @RolesAllowed("ADMIN")
    public Response updateBrand(@PathParam("id") UUID id, BrandUpdateDto brandDto)
    {
        return Response.ok(brandService.updateBrand(id, brandDto)).build();
    }

    /**
     * Deletes a brand with the specified ID.
     *
     * @param id The ID of the brand to delete.
     * @return A Response object indicating the success or failure of the deletion operation.
     */
    @DELETE
    @Path("/{id}")
    @Produces("application/json")
    @RolesAllowed("ADMIN")
    public Response deleteBrand(@PathParam("id") UUID id)
    {
        brandService.deleteBrand(id);
        return Response.noContent().build();
    }

}
