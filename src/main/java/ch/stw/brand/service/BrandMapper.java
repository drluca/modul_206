package ch.stw.brand.service;

import ch.stw.brand.dto.BrandCreateDto;
import ch.stw.brand.dto.BrandResponseDto;
import ch.stw.brand.dto.BrandUpdateDto;
import ch.stw.brand.entity.Brand;
import ch.stw.common.service.MappingConfig;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(config = MappingConfig.class)
public interface BrandMapper
{
    Brand toEntity(BrandResponseDto brandResponseDto);

    Brand toEntity(BrandCreateDto brandCreateDto);

    BrandResponseDto toDto(Brand brand);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    Brand partialUpdate(
        BrandUpdateDto brandUpdateDto,
        @MappingTarget Brand brand);
}
