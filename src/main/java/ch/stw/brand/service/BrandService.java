package ch.stw.brand.service;

import ch.stw.brand.dto.BrandCreateDto;
import ch.stw.brand.dto.BrandResponseDto;
import ch.stw.brand.dto.BrandUpdateDto;
import ch.stw.brand.entity.Brand;
import ch.stw.brand.repository.BrandRepository;
import jakarta.enterprise.context.RequestScoped;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;

@RequestScoped
@RequiredArgsConstructor
public class BrandService
{

    private final BrandRepository brandRepository;

    private final BrandMapper brandMapper;

    private final UriBuilder uriBuilder = UriBuilder.fromPath("/brands/");

    /**
     * Retrieves a list of brands.
     *
     * @return The list of brands as a list of {@link BrandResponseDto} objects.
     */
    public List<BrandResponseDto> getBrands()
    {
        List<Brand> brands = brandRepository.listAll();
        return brands.stream().map(brandMapper::toDto).toList();
    }

    /**
     * Retrieves a brand by its ID.
     *
     * @param id The ID of the brand.
     * @return The brand as a {@link BrandResponseDto} object.
     * @throws NotFoundException if the brand is not found.
     */
    public BrandResponseDto getBrand(UUID id)
    {
        Brand brand = brandRepository.findByIdOptional(id)
            .orElseThrow(() -> new NotFoundException("Brand not found"));
        return brandMapper.toDto(brand);
    }

    /**
     * Creates a new brand.
     *
     * @param brandDto The brand creation DTO.
     * @return The URI of the newly created brand.
     */
    @Transactional
    public URI createBrand(BrandCreateDto brandDto)
    {
        final Brand brand = brandMapper.toEntity(brandDto);
        brandRepository.persistAndFlush(brand);
        return uriBuilder.path("{id}").build(brand.getId());
    }

    /**
     * Updates a brand with the specified ID.
     *
     * @param id             The ID of the brand to update.
     * @param brandUpdateDto The BrandUpdateDto object containing the updated brand information.
     * @return The updated brand as a BrandResponseDto object.
     * @throws NotFoundException if the brand is not found.
     */
    @Transactional
    public BrandResponseDto updateBrand(UUID id, BrandUpdateDto brandUpdateDto)
    {
        Brand brand = brandRepository.findByIdOptional(id)
            .orElseThrow(() -> new NotFoundException("Brand not found"));
        brandMapper.partialUpdate(brandUpdateDto, brand);
        return brandMapper.toDto(brand);
    }

    /**
     * Deletes a brand with the specified ID.
     *
     * @param id The ID of the brand to delete.
     * @throws NotFoundException if the brand is not found.
     */
    @Transactional
    public void deleteBrand(UUID id)
    {
        Brand brand = brandRepository.findByIdOptional(id)
            .orElseThrow(() -> new NotFoundException("Brand not found"));
        brandRepository.delete(brand);
    }
}
