package ch.stw.common.dto;

public record PageRequest(int page, int size)
{
}
