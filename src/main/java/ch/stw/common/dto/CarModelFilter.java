package ch.stw.common.dto;

import java.util.UUID;

public record CarModelFilter(UUID brandId)
{
}
