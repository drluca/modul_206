package ch.stw.common.service;

import ch.stw.common.entity.BaseEntity;
import java.lang.reflect.InvocationTargetException;
import java.util.UUID;
import org.mapstruct.Mapper;
import org.mapstruct.TargetType;

@EntityReferenceMapper
@Mapper(componentModel = "jakarta")
public class EntityReferenceMapperImpl
{

    @EntityIdReference
    public <T extends BaseEntity> T getReference(UUID id, @TargetType Class<T> entityClass)
        throws NoSuchMethodException, InvocationTargetException, InstantiationException,
        IllegalAccessException
    {
        if (id == null)
        {
            return null;
        }
        T reference = entityClass.getDeclaredConstructor().newInstance();
        reference.setId(id);
        return reference;
    }
}
