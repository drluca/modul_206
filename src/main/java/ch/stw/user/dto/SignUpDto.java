package ch.stw.user.dto;

import jakarta.validation.constraints.Email;
import java.io.Serializable;

/**
 * DTO for {@link ch.stw.user.entity.User}
 */
public record SignUpDto(@Email String username, String password) implements Serializable
{
}
