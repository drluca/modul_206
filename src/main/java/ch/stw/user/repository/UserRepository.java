package ch.stw.user.repository;

import ch.stw.user.entity.User;
import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;
import jakarta.enterprise.context.ApplicationScoped;
import java.util.Optional;
import java.util.UUID;

@ApplicationScoped
public class UserRepository implements PanacheRepositoryBase<User, UUID>
{

    public Optional<User> findByUsernameOptional(String username)
    {
        return find("username", username).firstResultOptional();
    }

}
