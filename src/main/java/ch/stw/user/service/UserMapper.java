package ch.stw.user.service;

import ch.stw.user.dto.SignUpDto;
import ch.stw.user.entity.User;
import io.quarkus.elytron.security.common.BcryptUtil;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants.ComponentModel;
import org.mapstruct.Named;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = ComponentModel.CDI)
public interface UserMapper
{

    @Mapping(target = "role", constant = "USER")
    @Mapping(target = "password", source = "password", qualifiedByName = "hashPassword")
    User toEntity(SignUpDto signUpDto);

    @Named("hashPassword")
    default String hashPassword(String password)
    {
        return BcryptUtil.bcryptHash(password);
    }
}
