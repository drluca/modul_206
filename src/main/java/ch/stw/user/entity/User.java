package ch.stw.user.entity;

import ch.stw.common.entity.BaseEntity;
import io.quarkus.security.jpa.Password;
import io.quarkus.security.jpa.Roles;
import io.quarkus.security.jpa.UserDefinition;
import io.quarkus.security.jpa.Username;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.validation.constraints.Email;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@RequiredArgsConstructor
@Getter
@Setter
@UserDefinition
public class User extends BaseEntity
{
    @Column(name = "username")
    @Username
    @Email
    private String username;

    @Column(name = "password")
    @Password
    private String password;

    @Column(name = "role")
    @Roles
    private String role;
}
