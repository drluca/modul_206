package ch.stw.carmodel.resource;

import ch.stw.carmodel.dto.CarModelCreateDto;
import ch.stw.carmodel.dto.CarModelUpdateDto;
import ch.stw.carmodel.service.CarModelService;
import ch.stw.common.dto.CarModelFilter;
import jakarta.annotation.security.RolesAllowed;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.PATCH;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Response;
import java.util.UUID;
import lombok.RequiredArgsConstructor;

@Path("models")
@ApplicationScoped
@RequiredArgsConstructor
public class CarModelResource
{

    private final CarModelService carModelService;

    @GET
    @Produces("application/json")
    @RolesAllowed({"USER", "ADMIN"})
    public Response getCarModels(@QueryParam("brandId") UUID brandId)
    {

        CarModelFilter carModelFilter = new CarModelFilter(brandId);

        return Response.ok(carModelService.getCarModels(carModelFilter)).build();
    }

    @GET
    @Path("{id}")
    @Produces("application/json")
    @RolesAllowed({"USER", "ADMIN"})
    public Response getCarModel(@PathParam("id") UUID id)
    {
        return Response.ok(carModelService.getCarModel(id)).build();
    }

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @RolesAllowed("ADMIN")
    public Response createCarModel(CarModelCreateDto carModelCreateDto)
    {
        return Response.created(carModelService.createCarModel(carModelCreateDto)).build();
    }

    @PATCH
    @Path("{id}")
    @Consumes("application/json")
    @Produces("application/json")
    @RolesAllowed("ADMIN")
    public Response updateCarModel(@PathParam("id") UUID id, CarModelUpdateDto carModelUpdateDto)
    {
        return Response.ok(carModelService.updateCarModel(id, carModelUpdateDto)).build();
    }

    @DELETE
    @Path("{id}")
    @Produces("application/json")
    @RolesAllowed("ADMIN")
    public Response deleteCarModel(@PathParam("id") UUID id)
    {
        carModelService.deleteCarModel(id);
        return Response.noContent().build();
    }
}
