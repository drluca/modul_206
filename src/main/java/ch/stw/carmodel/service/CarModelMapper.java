package ch.stw.carmodel.service;

import ch.stw.brand.service.BrandMapper;
import ch.stw.carmodel.dto.CarModelCreateDto;
import ch.stw.carmodel.dto.CarModelResponseDto;
import ch.stw.carmodel.dto.CarModelUpdateDto;
import ch.stw.carmodel.entity.CarModel;
import ch.stw.common.service.EntityIdReference;
import ch.stw.common.service.EntityReferenceMapper;
import ch.stw.common.service.MappingConfig;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(config = MappingConfig.class, uses = BrandMapper.class)
public interface CarModelMapper
{
    CarModel toEntity(CarModelResponseDto carModelResponseDto);

    @Mapping(source = "brandId", target = "brand", qualifiedBy = {EntityReferenceMapper.class,
        EntityIdReference.class})
    CarModel toEntity(CarModelCreateDto carModelCreateDto);

    @Mapping(source = "brand.id", target = "brandId")
    CarModelResponseDto toDto(CarModel carModel);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    @Mapping(source = "brandId", target = "brand", qualifiedBy = {EntityReferenceMapper.class,
        EntityIdReference.class})
    CarModel partialUpdate(
        CarModelUpdateDto carModelDto,
        @MappingTarget CarModel carModel);

}
