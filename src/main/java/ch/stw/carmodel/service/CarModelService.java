package ch.stw.carmodel.service;

import ch.stw.carmodel.dto.CarModelCreateDto;
import ch.stw.carmodel.dto.CarModelResponseDto;
import ch.stw.carmodel.dto.CarModelUpdateDto;
import ch.stw.carmodel.entity.CarModel;
import ch.stw.carmodel.repository.CarModelRepository;
import ch.stw.common.dto.CarModelFilter;
import io.quarkus.panache.common.Parameters;
import jakarta.enterprise.context.RequestScoped;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;

@RequestScoped
@RequiredArgsConstructor
public class CarModelService
{

    private final CarModelRepository carModelRepository;

    private final CarModelMapper carModelMapper;

    private final UriBuilder uriBuilder = UriBuilder.fromPath("/models/");

    public List<CarModelResponseDto> getCarModels(CarModelFilter carModelFilter)
    {
        List<CarModel> carModels = carModelRepository.findAll()
            .filter("brandFilter", Parameters.with("brandId", carModelFilter.brandId()))
            .list();

        return carModels.stream().map(carModelMapper::toDto).toList();
    }

    public CarModelResponseDto getCarModel(UUID id)
    {
        CarModel carModel = carModelRepository.findByIdOptional(id)
            .orElseThrow(() -> new NotFoundException("Car model not found"));
        return carModelMapper.toDto(carModel);
    }

    @Transactional
    public URI createCarModel(CarModelCreateDto carModelCreateDto)
    {
        CarModel carModel = carModelMapper.toEntity(carModelCreateDto);
        carModelRepository.persist(carModel);
        return uriBuilder.path("{id}").build(carModel.getId());
    }

    @Transactional
    public CarModelResponseDto updateCarModel(UUID id, CarModelUpdateDto carModelUpdateDto)
    {
        CarModel carModel = carModelRepository.findByIdOptional(id)
            .orElseThrow(() -> new NotFoundException("Car model not found"));
        carModelMapper.partialUpdate(carModelUpdateDto, carModel);
        return carModelMapper.toDto(carModel);
    }

    @Transactional
    public void deleteCarModel(UUID id)
    {
        CarModel carModel = carModelRepository.findByIdOptional(id)
            .orElseThrow(() -> new NotFoundException("Car model not found"));
        carModelRepository.delete(carModel);
    }
}
