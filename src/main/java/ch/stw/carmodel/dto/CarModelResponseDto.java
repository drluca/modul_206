package ch.stw.carmodel.dto;

import java.io.Serializable;
import java.time.Year;
import java.util.UUID;

/**
 * DTO for {@link ch.stw.carmodel.entity.CarModel}
 */
public record CarModelResponseDto(UUID id, String name, Year yearOfConstruction, UUID brandId)
    implements Serializable
{
}
