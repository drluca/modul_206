package ch.stw.carmodel.entity;

import ch.stw.brand.entity.Brand;
import ch.stw.common.entity.BaseEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import java.time.Year;
import java.util.Objects;
import java.util.UUID;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.FilterDefs;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;
import org.hibernate.proxy.HibernateProxy;

@Entity
@RequiredArgsConstructor
@Getter
@Setter
@FilterDefs(@FilterDef(name = "brandFilter", parameters = @ParamDef(name = "brandId", type = UUID.class)))
@Filters(@Filter(name = "brandFilter", condition = "brand_id = :brandId"))
public class CarModel extends BaseEntity
{

    @NotNull(message = "Model name cant be empty")
    @NotEmpty(message = "Model name cant be empty")
    @NotBlank(message = "Model name cant be empty")
    @Column(name = "name", unique = true)
    private String name;

    @Column(name = "year_of_construction")
    private Year yearOfConstruction;

    @ManyToOne(optional = false)
    @JoinColumn(name = "brand_id", nullable = false)
    private Brand brand;

    @Override
    public final boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null)
        {
            return false;
        }
        Class<?> oEffectiveClass = o instanceof HibernateProxy ?
            ((HibernateProxy) o).getHibernateLazyInitializer().getPersistentClass() :
            o.getClass();
        Class<?> thisEffectiveClass = this instanceof HibernateProxy ?
            ((HibernateProxy) this).getHibernateLazyInitializer().getPersistentClass() :
            this.getClass();
        if (thisEffectiveClass != oEffectiveClass)
        {
            return false;
        }
        CarModel carModel = (CarModel) o;
        return getId() != null && Objects.equals(getId(), carModel.getId());
    }

    @Override
    public final int hashCode()
    {
        return this instanceof HibernateProxy ?
            ((HibernateProxy) this).getHibernateLazyInitializer().getPersistentClass().hashCode() :
            getClass().hashCode();
    }
}
