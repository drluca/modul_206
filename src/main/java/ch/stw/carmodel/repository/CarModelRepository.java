package ch.stw.carmodel.repository;

import ch.stw.carmodel.entity.CarModel;
import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;
import jakarta.enterprise.context.ApplicationScoped;
import java.util.UUID;

@ApplicationScoped
public class CarModelRepository implements PanacheRepositoryBase<CarModel, UUID>
{
}
