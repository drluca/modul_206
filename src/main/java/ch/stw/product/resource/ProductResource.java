package ch.stw.product.resource;

import ch.stw.common.dto.PageRequest;
import ch.stw.common.dto.ProductFilter;
import ch.stw.product.dto.ProductCreateDto;
import ch.stw.product.dto.ProductUpdateDto;
import ch.stw.product.service.ProductService;
import jakarta.annotation.security.RolesAllowed;
import jakarta.enterprise.context.RequestScoped;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.PATCH;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Response;
import java.util.UUID;
import lombok.RequiredArgsConstructor;

@Path("products")
@RequestScoped
@RequiredArgsConstructor
public class ProductResource
{

    private final ProductService productService;

    @GET
    @Produces("application/json")
    @RolesAllowed({"USER", "ADMIN"})
    public Response getProducts(
        @QueryParam("page") @DefaultValue("0") int page,
        @QueryParam("size") @DefaultValue("10") int size,
        @QueryParam("carModelId") UUID carModelId
    )
    {
        PageRequest pageRequest = new PageRequest(page, size);
        ProductFilter filter = new ProductFilter(carModelId);

        return Response.ok(productService.getProducts(pageRequest, filter)).build();
    }

    @GET
    @Path("{id}")
    @Produces("application/json")
    @RolesAllowed({"USER", "ADMIN"})
    public Response getProduct(@PathParam("id") UUID id)
    {
        return Response.ok(productService.getProduct(id)).build();
    }

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @RolesAllowed("ADMIN")
    public Response createProduct(ProductCreateDto productCreateDto)
    {
        return Response.created(productService.createProduct(productCreateDto)).build();
    }

    @PATCH
    @Path("{id}")
    @Consumes("application/json")
    @Produces("application/json")
    @RolesAllowed("ADMIN")
    public Response updateProduct(@PathParam("id") UUID id, ProductUpdateDto productUpdateDto)
    {
        return Response.ok(productService.updateProduct(id, productUpdateDto)).build();
    }

    @DELETE
    @Path("{id}")
    @Produces("application/json")
    @RolesAllowed("ADMIN")
    public Response deleteProduct(@PathParam("id") UUID id)
    {
        productService.deleteProduct(id);
        return Response.noContent().build();
    }
}
