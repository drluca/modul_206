package ch.stw.product.dto;

import java.io.Serializable;
import java.util.UUID;

/**
 * DTO for {@link ch.stw.product.entity.Product}
 */
public record ProductCreateDto(String name, String description, double price, UUID carModelId,
                               String imageUrl) implements Serializable
{
}
