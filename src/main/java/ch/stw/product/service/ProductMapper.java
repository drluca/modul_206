package ch.stw.product.service;

import ch.stw.carmodel.service.CarModelMapper;
import ch.stw.common.service.EntityIdReference;
import ch.stw.common.service.EntityReferenceMapper;
import ch.stw.common.service.MappingConfig;
import ch.stw.product.dto.ProductCreateDto;
import ch.stw.product.dto.ProductResponseDto;
import ch.stw.product.dto.ProductUpdateDto;
import ch.stw.product.entity.Product;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(config = MappingConfig.class, uses = CarModelMapper.class)
public interface ProductMapper
{
    @Mapping(source = "carModelId", target = "carModel", qualifiedBy = {EntityReferenceMapper.class,
        EntityIdReference.class})
    Product toEntity(ProductCreateDto productCreateDto);

    @Mapping(source = "carModel.id", target = "carModelId")
    ProductResponseDto toDto(Product product);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    @Mapping(source = "carModelId", target = "carModel", qualifiedBy = {EntityReferenceMapper.class,
        EntityIdReference.class})
    Product partialUpdate(
        ProductUpdateDto productResponseDto,
        @MappingTarget Product product);
}
