package ch.stw.product.service;

import ch.stw.common.dto.PageRequest;
import ch.stw.common.dto.ProductFilter;
import ch.stw.product.dto.ProductCreateDto;
import ch.stw.product.dto.ProductResponseDto;
import ch.stw.product.dto.ProductUpdateDto;
import ch.stw.product.entity.Product;
import ch.stw.product.repository.ProductRepository;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Parameters;
import jakarta.enterprise.context.RequestScoped;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;

@RequestScoped
@RequiredArgsConstructor
public class ProductService
{

    private final ProductRepository productRepository;

    private final ProductMapper productMapper;


    private final UriBuilder uriBuilder = UriBuilder.fromPath("/models/");

    public List<ProductResponseDto> getProducts(PageRequest pageRequest, ProductFilter productFilter)
    {
        PanacheQuery<Product> productsQuery = productRepository.findAll();

        if (productFilter.carModelId() != null)
        {
            productsQuery = productsQuery.filter("carModelFilter",
                Parameters.with("carModelId", productFilter.carModelId()));
        }

        List<Product> products = productsQuery
            .page(pageRequest.page(), pageRequest.size())
            .list();

        return products.stream().map(productMapper::toDto).toList();
    }

    public ProductResponseDto getProduct(UUID id)
    {
        Product product = productRepository.findByIdOptional(id)
            .orElseThrow(() -> new NotFoundException("Product not found"));
        return productMapper.toDto(product);
    }

    @Transactional
    public URI createProduct(ProductCreateDto productCreateDto)
    {
        Product product = productMapper.toEntity(productCreateDto);
        productRepository.persist(product);
        return uriBuilder.path("{id}").build(product.getId());
    }

    @Transactional
    public ProductResponseDto updateProduct(UUID id, ProductUpdateDto productResponseDto)
    {
        Product product = productRepository.findByIdOptional(id)
            .orElseThrow(() -> new NotFoundException("Product not found"));
        productMapper.partialUpdate(productResponseDto, product);
        return productMapper.toDto(product);
    }

    @Transactional
    public void deleteProduct(UUID id)
    {
        Product product = productRepository.findByIdOptional(id)
            .orElseThrow(() -> new NotFoundException("Product not found"));
        productRepository.delete(product);
    }
}
