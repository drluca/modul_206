package ch.stw.auth.service;

import ch.stw.auth.dto.AuthResponseDto;
import ch.stw.auth.dto.CredentialsDto;
import ch.stw.user.dto.SignUpDto;
import ch.stw.user.entity.User;
import ch.stw.user.repository.UserRepository;
import ch.stw.user.service.UserMapper;
import io.quarkus.elytron.security.common.BcryptUtil;
import io.quarkus.security.UnauthorizedException;
import io.smallrye.jwt.build.Jwt;
import jakarta.enterprise.context.RequestScoped;
import jakarta.transaction.Transactional;
import java.util.Optional;
import java.util.Set;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequestScoped
@RequiredArgsConstructor
@Slf4j
public class AuthService
{

    private final UserRepository userRepository;

    private final UserMapper userMapper;

    /**
     * Signs up a user with the provided sign-up details.
     *
     * @param signUpDto The SignUpDto object containing the user's sign-up details.
     * @return The AuthResponseDto object with the generated token.
     */
    @Transactional
    public AuthResponseDto signUp(SignUpDto signUpDto)
    {
        User user = userMapper.toEntity(signUpDto);
        userRepository.persist(user);
        return new AuthResponseDto(generateToken(user));
    }

    /**
     * Signs in a user with the provided credentials.
     *
     * @param credentialsDto The CredentialsDto object containing the user's credentials.
     * @return The AuthResponseDto object with the generated token.
     * @throws UnauthorizedException If the provided credentials are invalid.
     */
    public AuthResponseDto signIn(CredentialsDto credentialsDto)
    {
        Optional<User> user = userRepository.findByUsernameOptional(credentialsDto.username());
        if (user.isPresent() && verifyPassword(credentialsDto.password(), user.get().getPassword()))
        {
            return new AuthResponseDto(generateToken(user.get()));
        }
        throw new UnauthorizedException("Invalid credentials");
    }

    /**
     * Verifies if the input password matches the stored password using BcryptUtil.
     *
     * @param inputPassword  The input password to be verified.
     * @param storedPassword The stored password to be compared against.
     * @return {@code true} if the input password matches the stored password, {@code false} otherwise.
     */
    private boolean verifyPassword(String inputPassword, String storedPassword)
    {
        boolean matches = BcryptUtil.matches(inputPassword, storedPassword);
        log.info("Password verified: {}", matches);
        return matches;
    }

    /**
     * Generates a JWT token for the given user.
     *
     * @param user The User object for which the token is generated.
     * @return The generated JWT token as a string.
     */
    private String generateToken(User user)
    {
        return Jwt.issuer("https://stw.ch/issuer")
            .upn(user.getUsername())
            .groups(Set.of(user.getRole()))
            .sign();
    }
}
