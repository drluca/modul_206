package ch.stw.auth.dto;

public record AuthResponseDto(String jwtToken)
{
}
