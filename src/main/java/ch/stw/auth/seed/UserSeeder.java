package ch.stw.auth.seed;

import ch.stw.user.dto.SignUpDto;
import ch.stw.user.entity.User;
import ch.stw.user.repository.UserRepository;
import ch.stw.user.service.UserMapper;
import io.quarkus.runtime.StartupEvent;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.event.Observes;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;

/**
 * The UserSeeder class is responsible for seeding the database with an initial admin user,
 * if no users exist in the UserRepository.
 */
@ApplicationScoped
@RequiredArgsConstructor
public class UserSeeder
{

    /**
     * The UserRepository class is responsible for handling database operations related to the User entity.
     */
    private final UserRepository userRepository;

    /**
     * The UserMapper interface is responsible for mapping data between SignUpDto
     * and User entities.
     */
    private final UserMapper userMapper;

    /**
     * Seeds the database with an initial admin user if no users exist in the UserRepository.
     *
     * @param ev The startup event that triggers the method.
     */
    @Transactional
    public void seedAdmin(@Observes StartupEvent ev)
    {
        if (this.userRepository.count() == 0)
        {
            SignUpDto signUpDto = new SignUpDto(
                "admin",
                "stwadmin"
            );
            User user = userMapper.toEntity(signUpDto);
            user.setRole("ADMIN");
            userRepository.persistAndFlush(user);
        }
    }
}
