package ch.stw.auth.resource;

import ch.stw.auth.dto.CredentialsDto;
import ch.stw.auth.service.AuthService;
import ch.stw.user.dto.SignUpDto;
import jakarta.annotation.security.PermitAll;
import jakarta.enterprise.context.RequestScoped;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import lombok.RequiredArgsConstructor;

@Path("auth")
@RequestScoped
@RequiredArgsConstructor
public class AuthResource
{
    private final AuthService authService;

    /**
     * API endpoint for user sign-up.
     *
     * @param signUpDto The SignUpDto object containing the user's sign-up details.
     * @return A Response object indicating the success of the sign-up process.
     */
    @POST
    @PermitAll
    @Path("sign-up")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response signUp(SignUpDto signUpDto)
    {
        return Response.ok(authService.signUp(signUpDto)).build();
    }

    /**
     * Signs in a user with the provided credentials.
     *
     * @param credentialsDto The CredentialsDto object containing the user's credentials.
     * @return A Response object indicating the success of the sign-in process.
     */
    @POST
    @PermitAll
    @Path("sign-in")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response signIn(CredentialsDto credentialsDto)
    {
        return Response.ok(authService.signIn(credentialsDto)).build();
    }
}
