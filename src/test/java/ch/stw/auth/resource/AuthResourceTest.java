package ch.stw.auth.resource;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import ch.stw.auth.dto.AuthResponseDto;
import ch.stw.auth.dto.CredentialsDto;
import ch.stw.auth.service.AuthService;
import ch.stw.user.dto.SignUpDto;
import jakarta.ws.rs.core.Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class AuthResourceTest
{
    @Mock
    private AuthService authService;

    @InjectMocks
    private AuthResource authResource;

    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testSignUp() {
        SignUpDto signUpDto = new SignUpDto("testuser", "password");
        AuthResponseDto mockResponse = new AuthResponseDto("testtoken");

        when(authService.signUp(signUpDto)).thenReturn(mockResponse);

        Response response = authResource.signUp(signUpDto);

        assertEquals(200, response.getStatus());
        assertNotNull(response.getEntity());
        verify(authService, times(1)).signUp(signUpDto);
    }

    @Test
    void testSignIn() {
        CredentialsDto credentialsDto = new CredentialsDto("testuser", "password");
        AuthResponseDto mockResponse = new AuthResponseDto("testtoken");

        when(authService.signIn(credentialsDto)).thenReturn(mockResponse);

        Response response = authResource.signIn(credentialsDto);

        assertEquals(200, response.getStatus());
        assertNotNull(response.getEntity());
        verify(authService, times(1)).signIn(credentialsDto);
    }
}
