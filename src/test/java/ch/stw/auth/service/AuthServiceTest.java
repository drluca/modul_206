package ch.stw.auth.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import ch.stw.auth.dto.AuthResponseDto;
import ch.stw.auth.dto.CredentialsDto;
import ch.stw.user.dto.SignUpDto;
import ch.stw.user.entity.User;
import ch.stw.user.repository.UserRepository;
import ch.stw.user.service.UserMapper;
import io.quarkus.elytron.security.common.BcryptUtil;
import io.quarkus.security.UnauthorizedException;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class AuthServiceTest
{
    @Mock
    private UserRepository userRepository;

    @Mock
    private UserMapper userMapper;

    @InjectMocks
    private AuthService authService;

    @BeforeEach
    void setup()
    {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testSignUp() {
        SignUpDto signUpDto = new SignUpDto("testuser", "password");
        User mockUser = new User();
        mockUser.setUsername(signUpDto.username());
        mockUser.setPassword(BcryptUtil.bcryptHash(signUpDto.password())); // Proper bcrypt hash
        mockUser.setRole("USER"); // Ensure role is set

        when(userMapper.toEntity(signUpDto)).thenReturn(mockUser);

        AuthResponseDto response = authService.signUp(signUpDto);

        assertNotNull(response);
        verify(userRepository, times(1)).persist(mockUser);
    }

    @Test
    void testSignInUserNotFound()
    {
        CredentialsDto credentialsDto = new CredentialsDto("nonexistentuser", "password");

        when(userRepository.findByUsernameOptional("nonexistentuser")).thenReturn(Optional.empty());

        assertThrows(UnauthorizedException.class, () -> authService.signIn(credentialsDto));
    }
}
