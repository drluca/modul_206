## Gitflow Workflow

Dieser Workflow beschreibt die Vorgehensweise für die Entwicklung und das Release-Management in diesem Repository.

### Branches

1. **Main Branch** (`main`):  
   Der stabile Hauptzweig, der den Produktionsstand repräsentiert. Änderungen werden nur nach einem erfolgreichen Release hierhin gemerged.

2. **Develop Branch** (`develop`):  
   Der Hauptentwicklungszweig. Hier werden Feature-Branches und andere Änderungen zusammengeführt, bevor sie veröffentlicht werden.

3. **Feature Branches** (`feature/<name>`):  
   Für neue Features wird ein separater Branch erstellt, z. B. `feature/neues-feature`. Änderungen aus Feature-Branches werden über einen Merge Request in den `develop`-Branch integriert.

4. **Release Branches** (`release/<version>`):  
   Wenn ein Release vorbereitet wird, wird ein Release-Branch vom `develop`-Branch erstellt, z. B. `release/1.0.0`. Dieser Branch dient zur Durchführung von Tests und finalen Anpassungen.

5. **Hotfix Branches** (`hotfix/<name>`):  
   Für kritische Fehler in der Produktionsumgebung wird ein Hotfix-Branch vom `main`-Branch erstellt und nach Fertigstellung zurück in `main` und `develop` gemerged.

### Workflow

1. **Feature Development**:  
   - Erstelle einen Feature-Branch vom `develop`-Branch: `git checkout -b feature/<name>`.
   - Entwickle das Feature und halte die Commit-Nachrichten im [Conventional Commits Format](https://www.conventionalcommits.org/).
   - Stelle einen Merge Request in den `develop`-Branch.

2. **Release Vorbereitung**:  
   - Erstelle einen Release-Branch vom `develop`-Branch: `git checkout -b release/<version>`.
   - Teste und bereite das Release vor.
   - Erstelle einen Tag für das Release: `git tag -a v<version> -m "Release v<version>"`.

3. **Deployment**:  
   - Deploye das Artefakt basierend auf dem erstellten Tag.
   - Merge den Release-Branch nach erfolgreichem Deployment in `main`.

4. **Main Synchronisation**:  
   - Nach dem Deployment wird der Release-Branch ebenfalls in `develop` gemerged, um sicherzustellen, dass alle Änderungen synchronisiert sind.

### Commit Guidelines

- Nutze das [Conventional Commits Format](https://www.conventionalcommits.org/) für eine klare und strukturierte Historie.
  Beispiele:
  - `feat: Add new user authentication`
  - `fix: Resolve crash on startup`
  - `chore: Update dependencies`

### Tags

- Tags werden für Releases im Format `v<version>` erstellt, z. B. `v1.0.0`.

### Zusammenfassung der Branch-Namen

| Typ         | Namensschema         | Beschreibung                       |
|-------------|----------------------|------------------------------------|
| Main Branch | `main`               | Produktionsstand                   |
| Develop     | `develop`            | Hauptentwicklungszweig             |
| Feature     | `feature/<name>`     | Feature-Entwicklung                |
| Release     | `release/<version>`  | Vorbereitung eines Releases        |
| Hotfix      | `hotfix/<name>`      | Kritische Fehlerbehebungen         |

